<!DOCTYPE html>
<html>
    <head>
        <title>
            BITM Atomic Projects | Home Page
        </title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>

        <div class="container">
            <div class="" style="background-color: #01A9DB;">
                <div class="row">
                    <div class="col-md-3">
                        <img src="img/logo.png" height="80" style="margin: 40px 0 0 65px;"> 
                    </div>
                    <div class="col-md-9">
                        <img src="img/project_banner.jpg" width="100%" height="170">
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="index.php">Home</a></li>
                            <li><a href="views/Bitm/SEIP_116747/Book/index.php">Book</a></li>
                            <li><a href="views/Bitm/SEIP_116747/Mobile/index.php">Mobile</a></li> 
                            <li><a href="views/Bitm/SEIP_116747/Birthday/index.php">Birthday</a></li> 
                            <li><a href="views/Bitm/SEIP_116747/Gender/index.php">Gender</a></li> 
                            <li><a href="views/Bitm/SEIP_116747/Hobby/index.php">Hobby</a></li> 
                            <li><a href="views/Bitm/SEIP_116747/ProfilePicture/index.php">Profile Picture</a></li> 
                            <li><a href="views/Bitm/SEIP_116747/Subscription/index.php">Subcription</a></li> 
                            <li><a href="views/Bitm/SEIP_116747/Summary/index.php">Summary</a></li> 
                            
                        </ul>
                       
                    </div>
                </div>
            </nav>

            <div class="row">
                <div class="col-md-8">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="img/design_banner.jpg" alt="Chania">
                                <div class="carousel-caption">
                                    
                                </div>
                            </div>

                            <div class="item">
                                <img src="img/banner.jpg" alt="Chania">
                                <div class="carousel-caption">
                                    
                                </div>
                            </div>


                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <div class="panel" style="margin-top: 25px;">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="img/1415261415.jpg" alt="...">
                                <div class="caption">
                                    <h3>University</h3>
                                    <p>The Times Higher Education World University Rankings 2015-2016 list the best global universities and are the only international university performance tables to judge world class universities across all of their core </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="img/1415261370.jpg" alt="...">
                                <div class="caption">
                                    <h3>The Role of the University</h3>
                                    <p>Thank you, to my good friend Academy President Canny, to Provost Hegarty of Trinity College, members of the Royal Irish Academy, and honored guests. As I anticipated joining you here today, a story came to mind that involves the poet Robert Frost, when he was invited to read a poem at the presidential inauguration of John F. Kennedy, Harvard's most famous Irish-American graduate. At some point after the ceremony Frost supposedly leaned over to the new president and said, ""</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="img/1416294158.jpg" alt="...">
                                <div class="caption">
                                    <h3>Scholarship</h3>
                                    <p>The International Scholarship Search is the premier financial aid, college scholarship and international scholarship resource for students wishing to study abroad. At this site, you will find the most comprehensive listing of grants, scholarships, loan programs, and other information to assist college and university students in their pursuit to study abroad.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>


                </div>




                <div class="col-md-4">
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>






        </div>

        
        
        <footer>
            <div class="container" style=" height: 50px; color: #fff; padding-top: 15px; background-color:  #1b6d85; text-align: center;">
                <p>Xtreme Template @DipCoder</p>
            </div>
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>






