-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2016 at 09:38 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_crudprojectfeb16`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdays`
--

CREATE TABLE IF NOT EXISTS `birthdays` (
`id` int(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `b_place` varchar(150) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `birthdays`
--

INSERT INTO `birthdays` (`id`, `name`, `birthday`, `b_place`, `email_address`, `mobile`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SHAHIDA', '2016-03-18', 'HGHCV', '', '', '2016-03-28 14:00:40', '2016-04-07 09:33:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `book_image` blob NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author_name`, `book_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CGFDXGF', 'GFDGFXD', 0x6d6567616d696e642e6a7067, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10, 'DXXFDZF', 'ERWTDR', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(16, 'HDzEDS', 'WTDFG', 0x6d6567616d696e642e6a7067, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(20, 'FDHGSH', 'HDFDFX', 0x31323334313531325f313533313438353037333834373039325f3835343536343334373239313337363531365f6e2e6a7067, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(22, 'New Book', 'Pd author', 0x31313739363334365f313435373436303030373931363236365f313030373132353831343138343737393632335f6e2e6a7067, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-04 16:17:40'),
(23, 'Mahte Book', 'p book', 0x3934343338345f313038373035303630383030313635315f353637363532383734313733383638313734375f6e2e706e67, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-17 00:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE IF NOT EXISTS `genders` (
`id` int(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `gender` varchar(8) NOT NULL,
  `term_and_condition` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `name`, `gender`, `term_and_condition`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'THGFDX', 'Female', 1, '2016-03-28 13:54:08', '0000-00-00 00:00:00', NULL),
(2, 'Alex ', 'Male', 0, '2016-03-28 13:54:21', '0000-00-00 00:00:00', '2016-04-05 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`id` int(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hobby` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobby`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'TGDZSNGXZ', 'Cricket,Reading', '2016-03-28 12:43:40', '0000-00-00 00:00:00', NULL),
(2, 'Md. Saifullah', 'Football', '2016-03-28 12:51:55', '0000-00-00 00:00:00', '2016-04-05 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profilepictures`
--

CREATE TABLE IF NOT EXISTS `profilepictures` (
`id` int(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profilepictures`
--

INSERT INTO `profilepictures` (`id`, `name`, `profile_pic`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(13, 'JFDFHGFCG', '1460014589146001388814599608402.jpg', 0, '2016-04-07 13:36:29', '0000-00-00 00:00:00', NULL),
(14, 'VXFCFG', '146001460014599608914.jpg', 0, '2016-04-07 13:36:40', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE IF NOT EXISTS `subscriptions` (
`id` int(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email_address` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `name`, `email_address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'GFCZXFD', 'DXGF@gmail.com', '2016-04-06 00:12:32', '0000-00-00 00:00:00', NULL),
(2, 'HGZds', 'FXGFXC@gmail.comzz', '2016-04-06 00:16:27', '0000-00-00 00:00:00', NULL),
(3, 'Alex rouli', 'aaaa@gmail.com', '2016-04-06 00:16:39', '0000-00-00 00:00:00', '2016-04-05 00:00:00'),
(4, 'HGFseFGC', 'GFXFDC@gmail.com', '2016-04-06 00:17:26', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `summaries`
--

CREATE TABLE IF NOT EXISTS `summaries` (
`id` int(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `summary` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `summaries`
--

INSERT INTO `summaries` (`id`, `name`, `summary`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Organization ', 'SummarySummarySummarySummarySummarySummarySummarySummarySummarySummarySummarySummarySummarySummarySummary', '2016-04-06 01:30:18', '0000-00-00 00:00:00', '2016-04-05 00:00:00'),
(2, 'RGZSEGHCXDF', 'HGJGDRHFGX CG DHFFG', '2016-04-06 02:06:24', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mobiles`
--

CREATE TABLE IF NOT EXISTS `tbl_mobiles` (
`id` int(3) NOT NULL,
  `title` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  `m_image` blob NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_mobiles`
--

INSERT INTO `tbl_mobiles` (`id`, `title`, `model`, `m_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ERHGDF', 'HGXFB', '', '2016-03-19 15:19:20', '2016-04-07 09:34:23', NULL),
(3, 'THGFDZ', 'HGDZSFD', '', '2016-03-19 15:21:22', '2016-04-07 09:34:28', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdays`
--
ALTER TABLE `birthdays`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepictures`
--
ALTER TABLE `profilepictures`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summaries`
--
ALTER TABLE `summaries`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_mobiles`
--
ALTER TABLE `tbl_mobiles`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdays`
--
ALTER TABLE `birthdays`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `profilepictures`
--
ALTER TABLE `profilepictures`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `summaries`
--
ALTER TABLE `summaries`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_mobiles`
--
ALTER TABLE `tbl_mobiles`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
