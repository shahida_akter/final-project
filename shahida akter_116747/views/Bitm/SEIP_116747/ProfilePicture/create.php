<?php
include_once '../../../../header.php';
session_start();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Profile Picture | Create Page
        </title>
    </head>
    <body>
         <h1 align="center">Create Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        
        <form action="store.php" method="POST" enctype="multipart/form-data">
            <fieldset  style="text-align: left;">
                <legend>Add Your Profile Picture info</legend>
                <label>Enter Your Name:</label>
                <input type="text" name="name">
                <span>
                    <?php 
                        if(isset($_SESSION['namerr'])){
                            echo $_SESSION['namerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
               
                <label>Choose A Image:</label>
                <input type="file" name="profile_pic">
                <span>
                    <?php 
                        if(isset($_SESSION['imagerr'])){
                            echo $_SESSION['imagerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        
        </div>
    </body>
</html>
