<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116747\Hobby\hobby;

$hobbyobj = new hobby();

$id = $_GET['id'];
$single_view = $hobbyobj->view($id);

$exp = explode(',',$single_view['hobby']);
$single_view['hobby'] = $exp;
//print_r($single_view['name']);
//die();


?>
<html>
    <head>
        <title>
            Hobby | Edit Page
        </title>
    </head>
    <body>
        <h1 align="center">Edit Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="index.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        
        <form action="update.php" method="POST" >
            <fieldset style="text-align: left;">
                <legend>Edit Hobby Information Here</legend>
                <label>Enter Your Full Name:</label>
                <input type="text" name="name" value="<?php echo $single_view['name'];?>">
                <input type="hidden" name="id" value="<?php echo $single_view['id'];?>">
                <br>
                <label>Select Hobbies:</label>
                <input type="checkbox" name="hobby[]" value="Cricket" <?php if(in_array('Cricket',$single_view['hobby'])){echo 'checked';}?>>Cricket
                <input type="checkbox" name="hobby[]" value="Football" <?php if(in_array('Football',$single_view['hobby'])){echo 'checked';}?>>Football
                <input type="checkbox" name="hobby[]" value="Reading" <?php if(in_array('Reading',$single_view['hobby'])){echo 'checked';}?>>Reading
                <input type="checkbox" name="hobby[]" value="Gardening" <?php if(in_array('Gardening',$single_view['hobby'])){echo 'checked';}?>>Gardening
               
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        
        </div> 
    </body>
</html>