<?php
include_once '../../../../header.php';

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116747\Subscription\subscription;

$id = $_GET['id'];

$subobj = new subscription();
$Single_data = $subobj->view($id);
//echo '<pre>';
//print_r($SingleBook);
//exit();
?>
<html>
    <head>
        <title>
            Subscription | View Page
        </title>
    </head>
    <body>

        <h1 align="center">View Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            Homes
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>

                </div>
            </div>
        </div>

        <div class="content" align="center">

          
            <table border='1'style="font-size: 22px;">
                <th>ID</th>
                <th>Name</th>
                <th>Email Address</th>
                <th>Action</th>

                <tr>
                    <td><?php echo $Single_data["id"]; ?></td>
                    <td><?php echo $Single_data["name"]; ?></td>
                    <td><?php echo $Single_data["email_address"]; ?></td>
                    <td>
                        <a href="show.php?id=<?php echo $Single_data["id"]; ?>">
                            <button type="button" class="btn btn-primary">
                                <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                View
                            </button>
                        </a> | 
                        <a href="edit.php?id=<?php echo $Single_data["id"]; ?>">
                            <button type="button" class="btn btn-success">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                Edit
                            </button>
                        </a> | 
                        <a href="trash.php?id=<?php echo $Single_data["id"]; ?>">
                            <button type="button" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                Delete
                            </button>
                        </a>
                    </td>
                </tr>
            </table>

        </div>


    </body>
</html>
<!-- <?php include_once '../../../../footer.php'; ?> -->
