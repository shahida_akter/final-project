<?php

 include_once '../../../../vendor/autoload.php';
 
use App\Bitm\SEIP_116747\Actor\actor;
use App\Bitm\SEIP_116747\Utility\utility;

$actorobj = new actor();

$all_info = $actorobj->index();
 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Actor | Home Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="trashed.php">Deleted Items</a> 
        <?php 
            if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
                echo utility::message(NULL);
            }
        ?>
      
        <table border="1">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Name</th>
              <th>Actors</th>
              <th>Action</th>
            </thead>
            <tbody>
               <?php
                $s = 0;
                foreach($all_info as $v_info){
                 $s++;   
               ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $v_info['id'];?></td>
                    <td><?php echo $v_info['name'];?></td>
                    <td><?php echo $v_info['actor'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $v_info['id'];?>">View</a> |
                        <a href="edit.php?id=<?php echo $v_info['id'];?>">Edit</a> |
                        <a href="trash.php?id=<?php echo $v_info['id'];?>">Delete</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        
        
        
    </body>
</html>



