<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116747\Summary\summary;

$id = $_GET['id'];

$sumobj = new summary();
$Single_data = $sumobj->view($id);
//print_r($Single_data['email_address']);
//exit();
?>
<html>
    <head>
        <title>
            Book | Edit Page
        </title>
    </head>
    <body>
        <h1 align="center">Edit Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">

            <form action="update.php" method="POST">
                <fieldset>
                    <legend>Edit Organization's Summary INFO</legend>
                    <table>
                        <tr>
                            <td><label>Edit Your Name</label></td>
                            <td>
                                <input type="text" name="name" id="name" value="<?php echo $Single_data['name']; ?>">
                                <input type="hidden" name="id" value="<?php echo $Single_data['id']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td><label>Edit Email Address</label></td>
                            <td><textarea name="summary" rows="6" cols="40"><?php echo $Single_data['summary']; ?></textarea></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="submit" class="btn btn-success pull-right"  value="Update">
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>
    </body>
</html>
