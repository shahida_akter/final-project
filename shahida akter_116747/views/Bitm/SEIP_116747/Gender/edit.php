<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116747\Gender\gender;

$genderobj = new gender();

$id = $_GET['id'];
$singleData = $genderobj->view($id);
//print_r($singleData);
//exit();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Gender | Edit Page
        </title>
    </head>
    <body>
        <h1 align="center">Edit Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="index.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        
        <form action="update.php" method="POST" >
            <fieldset style="text-align: left;">
                <legend>Add Gender Information Here</legend>
                <label>Enter Your Full Name:</label>
                <input type="text" name="name" value="<?php echo $singleData['name'];?>">
                <input type="hidden" name="id" value="<?php echo $singleData['id'];?>">
                <br>
                <label>Select Gender:</label>
                Male: <input type="radio" name="gender" value="Male" <?php if($singleData['gender']=='Male') { echo 'checked'; }?>>
                Female:<input type="radio" name="gender" value="Female" <?php if($singleData['gender']=='Female') { echo 'checked'; }?>>
                <br>
                <input type="checkbox" name="term" value="1" <?php if($singleData['term_and_condition'] ==1){ echo 'checked';};?>> You should obey this term and conditionable
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        </div>
        
    </body>
</html>
