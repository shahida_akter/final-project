<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116747\Birthday\birthdayClass;
use App\Bitm\SEIP_116747\Utility\utility;

$birthdayobj = new birthdayClass();
$all_info = $birthdayobj->view();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Birthday | Home Page
        </title>
    </head>
    <body>
        <h1 align="center">Birthday Atomic Project</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" style="font-size: 22px; margin-left: 13%">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="trashed.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
                            Deleted Items
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As XL
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>
                    <select class="btn" style="font-size: 16px; border: 1px solid #dcdcdc;">
                        <option>Show</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
                </div>
            </div>
        </div>




        <div class="content" align="center">
            <?php
            if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
                //  echo $_SESSION['message'];
                echo utility::message(NULL);
            }
            ?>
            <table border="1" style="font-size: 20px;">
                <thead>
                <th>SI</th>
                <th>ID</th>
                <th>Name</th>
                <th>Birthday</th>
                <th>Birthday Place</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php
                    $s = 0;
                    foreach ($all_info as $v_info) {
                        $s++;
                        ?>
                        <tr>
                            <td><?php echo $s; ?></td>
                            <td><?php echo $v_info['id']; ?></td>
                            <td><?php echo $v_info['name']; ?></td>
                            <td><?php echo $v_info['birthday']; ?></td>
                            <td><?php echo $v_info['b_place']; ?></td>
                            <td>
                                <a href="show.php?id=<?php echo $v_info["id"]; ?>">
                                    <button type="button" class="btn btn-primary">
                                        <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                        View
                                    </button>
                                </a> | 
                                <a href="edit.php?id=<?php echo $v_info["id"]; ?>">
                                    <button type="button" class="btn btn-success">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                        Edit
                                    </button>
                                </a> | 
                                <a href="trash.php?id=<?php echo $v_info["id"]; ?>">
                                    <button type="button" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        Delete
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <nav>
                <ul class="pagination pagination-lg">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>

        </div>
    </body>
</html>


