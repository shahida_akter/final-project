<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116747\Book\BookClass_File;

$bookobj = new  BookClass_File();
$Alldata = $bookobj->trashed();

?>
<html>
    <head>
        <title>
            Book | Recycle Page
        </title>
    </head>
    <body>

        <h1 align="center">Book Atomic Project</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" style="font-size: 22px; margin-left: 13%">
                    <a href="index.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Books List
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As XL
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>
                    <select class="btn" style="font-size: 16px; border: 1px solid #dcdcdc;">
                        <option>Show</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
                </div>
            </div>
        </div>




        <div class="content" align="center">

            <?php
//        print_r($_SESSION);
//        exit();
            if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
                echo $_SESSION['message'];
                echo utility::message();
            }
            if (isset($_SESSION['mgs'])) {
                echo $_SESSION['mgs'];
                unset($_SESSION['mgs']);
            }
            ?>
            <table border='1'style="font-size: 22px;">
                <th>SI</th>
                <th>ID</th>
                <th>Title</th>
                <th>Author Name</th>
                <th>Action</th>
                <?php
                $serial = 0;
                foreach ($Alldata as $v_book) {
                    $serial++;
//                    var_dump($v_book);
//                    exit();
                    ?>
                    <tr>
                        <td><?php echo $serial; ?></td>
                        <td><?php echo $v_book["id"]; ?></td>
                        <td><?php echo $v_book["title"]; ?></td>
                        <td><?php echo $v_book["author_name"]; ?></td>
                        <td>
                            <a href="show.php?id=<?php echo $v_book["id"]; ?>">
                                <button type="button" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                    View
                                </button>
                            </a> | 
                            <a href="edit.php?id=<?php echo $v_book["id"]; ?>">
                                <button type="button" class="btn btn-success">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    Edit
                                </button>
                            </a> | 
                            <a href="restore.php?id=<?php echo $v_book["id"]; ?>">
                                <button type="button" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    Restore
                                </button>
                            </a> |
                            <a href="delete.php?id=<?php echo $v_book["id"]; ?>">
                                <button type="button" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    Delete
                                </button>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <nav>
                <ul class="pagination pagination-lg">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>


    </body>
</html>
<?php include_once '../../../../footer.php';?>
