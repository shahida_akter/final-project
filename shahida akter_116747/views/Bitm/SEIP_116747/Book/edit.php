<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116747\Book\BookClass_File;

$bookobj = new BookClass_File();
$id = $_GET['id'];
$ebook = $bookobj->viewSingleBook($id);
?>
<html>
    <head>
        <title>
            Book | Edit Page
        </title>
    </head>
    <body>
        <h1 align="center">Edit Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            Books List
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">

            <form action="update.php" method="POST">
                <fieldset>
                    <legend>Edit Your Favourite Book</legend>
                    <table>
                        <tr>
                            <td><label>Book Title</label></td>
                            <td>
                                <input type="text" name="title" id="title" value="<?php echo $ebook['title']; ?>">
                                <input type="hidden" name="id" value="<?php echo $ebook['id']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td><label>Author Name</label></td>
                            <td><input type="text" name="author_name" value="<?php echo $ebook['author_name']; ?>"></td>
                        </tr>
                        <tr>
                            <td><label>Select a Image</label></td>
                            <td><input type="file" name="book_image" value="<?php echo $ebook['book_image']; ?>"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="submit" class="btn btn-success pull-right" style="margin-right:140px; " value="Update">
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>
    </body>
</html>
