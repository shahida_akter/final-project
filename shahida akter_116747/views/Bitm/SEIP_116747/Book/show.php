<?php
include_once '../../../../header.php';

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116747\Book\BookClass_File;

$id = $_GET['id'];

$bookobj = new BookClass_File();
$SingleBook = $bookobj->viewSingleBook($id);
//echo '<pre>';
//print_r($SingleBook);
//exit();
?>
<html>
    <head>
        <title>
            Book | View Page
        </title>
    </head>
    <body>

        <h1 align="center">View Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            Books List
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>

                </div>
            </div>
        </div>

        <div class="content" align="center">

            <?php
//        print_r($_SESSION);
//        exit();
            if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
                echo $_SESSION['message'];
                echo utility::message();
            }
            if (isset($_SESSION['mgs'])) {
                echo $_SESSION['mgs'];
                unset($_SESSION['mgs']);
            }
            ?>
            <table border='1'style="font-size: 22px;">
                <th>ID</th>
                <th>Title</th>
                <th>Author Name</th>
                <th>Action</th>

                <tr>
                    <td><?php echo $SingleBook["id"]; ?></td>
                    <td><?php echo $SingleBook["title"]; ?></td>
                    <td><?php echo $SingleBook["author_name"]; ?></td>
                    <td>
                        <a href="show.php?id=<?php echo $SingleBook["id"]; ?>">
                            <button type="button" class="btn btn-primary">
                                <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                View
                            </button>
                        </a> | 
                        <a href="edit.php?id=<?php echo $SingleBook["id"]; ?>">
                            <button type="button" class="btn btn-success">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                Edit
                            </button>
                        </a> | 
                        <a href="trash.php?id=<?php echo $SingleBook["id"]; ?>">
                            <button type="button" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                Delete
                            </button>
                        </a>
                    </td>
                </tr>
            </table>

        </div>


    </body>
</html>
<!-- <?php include_once '../../../../footer.php'; ?> -->
