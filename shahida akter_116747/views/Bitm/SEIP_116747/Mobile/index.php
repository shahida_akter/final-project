<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116747\Mobile\mobileClass;
use App\Bitm\SEIP_116747\Utility\utility;

$mobileobj = new mobileClass();
$mobile_info = $mobileobj->view();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Home Page
        </title>
    </head>
    <body>
        <h1 align="center">Mobile Atomic Project</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" style="font-size: 22px; margin-left: 13%">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> |
                    <a href="trashed.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
                            Deleted Books
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As XL
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>
                    <select class="btn" style="font-size: 16px; border: 1px solid #dcdcdc;">
                        <option>Show</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
                </div>
            </div>
        </div>




        <div class="content" align="center">

            <?php
            if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
                //  echo $_SESSION['message'];
                echo utility::message(NULL);
            }
            ?>
            <table border="1" style="font-size: 22px;">
                <thead>
                <th>SI</th>
                <th>ID</th>
                <th>Mobile Title</th>
                <th>Mobile Model</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php
                    $s = 0;
                    foreach ($mobile_info as $v_mobile) {
                        $s++;
                        ?>
                        <tr>
                            <td><?php echo $s; ?></td>
                            <td><?php echo $v_mobile['id']; ?></td>
                            <td><?php echo $v_mobile['title']; ?></td>
                            <td><?php echo $v_mobile['model']; ?></td>
                            <td>
                                <a href="show.php?id=<?php echo $v_mobile["id"]; ?>">
                                    <button type="button" class="btn btn-primary">
                                        <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                        View
                                    </button>
                                </a> | 
                                <a href="edit.php?id=<?php echo $v_mobile["id"]; ?>">
                                    <button type="button" class="btn btn-success">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                        Edit
                                    </button>
                                </a> | 
                                <a href="trash.php?id=<?php echo $v_mobile["id"]; ?>">
                                    <button type="button" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        Delete
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <nav>
                <ul class="pagination pagination-lg">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

    </body>
</html>
<?php include_once '../../../../footer.php';?>