<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116747\Mobile\mobileClass;

$mobileobj = new mobileClass();
$id = $_GET['id'];
$single_view = $mobileobj->show($id);
//echo "<pre>";
//print_r($single_view);
//exit();
 
?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Details Page
        </title>
    </head>
    <body>
       <h2 align="center">View Page</h2><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            Books List
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>

                </div>
            </div>
        </div>

        <div class="content" align="center">
       
        <table border="1" style="font-size: 22px;">
            <thead>
              <th>ID</th>
              <th>Mobile Title</th>
              <th>Mobile Model</th>
              <th>Picture</th>
              <th>Action</th>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $single_view['id'];?></td>
                    <td><?php echo $single_view['title'];?></td>
                    <td><?php echo $single_view['model'];?></td>
                    <td><img src="show.php?id=<?php echo $single_view['id'];?>" alt="" title="Image"/></td>
                    <td>
                        <a href="show.php?id=<?php echo $single_view["id"]; ?>">
                            <button type="button" class="btn btn-primary">
                                <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                View
                            </button>
                        </a> | 
                        <a href="edit.php?id=<?php echo $single_view["id"]; ?>">
                            <button type="button" class="btn btn-success">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                Edit
                            </button>
                        </a> | 
                        <a href="trash.php?id=<?php echo $single_view["id"]; ?>">
                            <button type="button" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                Delete
                            </button>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
        
        </div>
        
    </body>
</html>

