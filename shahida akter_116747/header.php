<!DOCTYPE html>
<html>
    <head>
        <title>
            BITM Atomic Project | Home Page
        </title>
        <link rel="stylesheet" type="text/css" href="../../../../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../../css/custom.css">
    </head>
    <body>

        <div class="container">
            <div class="" style="background-color: #01A9DB;">
                <div class="row">
                    <div class="col-md-3">
                        <img src="../../../../img/logo.png" height="80" style="margin: 40px 0 0 65px;"> 
                    </div>
                    <div class="col-md-9">
                        <img src="../../../../img/project_banner.jpg" width="100%" height="170">
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="index.php">Home</a></li>
                            <li><a href="../../../../views/Bitm/SEIP_116747/Birthday/index.php">Birthday</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_116747/Book/index.php">Book</a></li>
                            <li><a href="../../../../views/Bitm/SEIP_116747/Mobile/index.php">Mobile</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_116747/Gender/index.php">Gender</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_116747/Hobby/index.php">Hobby</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_116747/ProfilePicture/index.php">Profile Picture</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_116747/Subscription/index.php">Subscription</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_116747/Summary/index.php">Summary</a></li> 
                            
                            

                        </ul>
                       
                    </div>
                </div>
            </nav>
